//
//  StoryModel.swift
//  MC2
//
//  Created by Felinda Gracia Lubis on 10/06/21.
//

import Foundation

struct StoryModel {
    var character: String?
    var story: String
    var choice: [Choice]?
    var nextStory: Int?
}

struct Choice {
    var choice: String
    var state: Bool
    var nextStory: Int?
}
