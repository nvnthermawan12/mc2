//
//  StoryWrapper.swift
//  MC2
//
//  Created by Felinda Gracia Lubis on 11/06/21.
//

import Foundation

struct StoryWrapper {
    var name: String
    var stories: [StoryModel]
}
