//
//  StoryManager.swift
//  MC2
//
//  Created by Felinda Gracia Lubis on 11/06/21.
//

import Foundation

class StoryManager {
    var storyArr = [
        // story warung
        StoryWrapper(name: "Warung", stories: [
            // 0
            StoryModel(character: "", story: "Sesampainya di Warung Kanjeng Mami", choice: nil, nextStory: 1),
            // 1
            StoryModel(character: "", story: "Mau pesen opo, Yo?", choice: nil, nextStory: 2),
            // 2
            StoryModel(character: "", story: "Hmm..", choice: [Choice(choice: "aku ora pesen sego goreng, Bu", state: false, nextStory: 3), Choice(choice: "aku kate pesen sego goreng, Bu", state: true, nextStory: 4)], nextStory: 3),
            // 3
            StoryModel(character: "", story: "Lah lapo mrene?!! @!*#!*@(*&@!(*#%^@!)@(!!", choice: nil, nextStory: nil),
            // 4
            StoryModel(character: "", story: "Yo, pedes opo ora?", choice: nil, nextStory: 5),
            // 5
            StoryModel(character: "", story: "Hmm..", choice: [Choice(choice: "Iyo, Lur", state: false, nextStory: 6), Choice(choice: "Nggih, Bu", state: true, nextStory: 7)], nextStory: 6),
            // 6
            StoryModel(character: "", story: "Lar lur lar lur, ta uleg lambemu.  @!*#!*@(*&@!(*#%^@!)@(!!", choice: nil, nextStory: nil),
            // 7
            StoryModel(character: "", story: "Lombok e piro?", choice: [Choice(choice: "Wedi", state: false, nextStory: 8), Choice(choice: "Prei", state: false, nextStory: 8), Choice(choice: "Sasi", state: false, nextStory: 8), Choice(choice: "Siji", state: true, nextStory: 9)], nextStory: 9),
            // 8
            StoryModel(character: "", story: "Ngomong opo to, Yo. Gak jelas blas @!*#!*@(*&@!(*#%^@!)@(!!", choice: nil, nextStory: nil),
            // 9
            StoryModel(character: "", story: "Ngombe ne opo?", choice: nil, nextStory: 10),
            // 10
            StoryModel(character: "", story: "Hmm..", choice: [Choice(choice: "Aku klempoken es teh manis, Bu.", state: false, nextStory: 11), Choice(choice: "Aku njaluk es teh manis, Bu.", state: true, nextStory: 12)], nextStory: 12),
            // 11
            StoryModel(character: "", story: "Durung ngombe kok wes klempoken @!*#!*@(*&@!(*#%^@!)@(!", choice: nil, nextStory: nil),
            // 12
            StoryModel(character: "", story: "Wes. Opo maneh?", choice: [Choice(choice: "Piro?", state: false, nextStory: 13), Choice(choice: "Sampun, Bu. Pinten?", state: true, nextStory: 14)], nextStory: 14),
            // 13
            StoryModel(character: "", story: "Piro piro. Ta kruwes lambemu @!*#!*@(*&@!(*#%^@!)@(!", choice: nil, nextStory: nil),
            // 14
            StoryModel(character: "", story: "Es teh mbe sego goreng, selawe ewu.", choice: nil, nextStory: 15),
            // 15
            StoryModel(character: "", story: "(Hmm.. Selawe ewu itu berapa ya?)", choice: [Choice(choice: "Rp15.000", state: false, nextStory: 16), Choice(choice: "Rp25.00", state: true, nextStory: 17)], nextStory: 17),
            // 16
            StoryModel(character: "", story: "Sing genah. Kurang iki @!*#!*@(*&@!(*#%^@!)@(!", choice: nil, nextStory: nil),
            // 17
            StoryModel(character: "", story: "Yo, suwun.", choice: nil, nextStory: nil)
        ])
        //story lainnya
    ]
}
