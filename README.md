# Boyo

<h2 align="center"><img src="./img/logo.png" width="70%"></h2>

<p> A native iOS App that helps in learning surabaya slangs

This app is for  students from out of Surabaya who have hard time because they don't understand local language  by providing common conversation simulation and slang glossary This is a solution to the challenge because it helps them to learn and experience  daily conversation with local friends. </p>

## Design

<h2 align="center"><img src="./img/hifi.png" width="80%"></h2>

[Sources Sketch](https://sketch.com/s/5df31dec-52c8-4fab-83da-72d56f0ab5be)

## Sources Codes

[SpriteKit](https://gitlab.com/nvnthermawan12/mc2/-/tree/master/SpriteKit)

[UiKit](https://gitlab.com/nvnthermawan12/mc2/-/tree/master/UIkit)

## Etc

[CBL](https://miro.com/app/board/o9J_lEbFnKc=/)

[Project Management](https://miro.com/app/board/o9J_lCW5FPM=/)