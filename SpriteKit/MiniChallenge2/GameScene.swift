//
//  GameScene.swift
//  MiniChallenge2
//
//  Created by Novianto Hermawan on 09/06/21.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    private var logo: SKSpriteNode!
    private var labelHalloRek: SKLabelNode!
    private var LabelAjakan: SKLabelNode!
        
    override func didMove(to view: SKView) {
        
       let background = SKSpriteNode(imageNamed: "backgroundWelcome")
        background.anchorPoint = CGPoint(x: 0, y: 0)
        background.position = CGPoint(x: 0, y: 0)
        background.zPosition = CGFloat(0)
        background.size = CGSize(width: size.width, height: size.height)
        addChild(background)
        
        bajolLogo()
        labelHalloRek = SKLabelNode(fontNamed: "Chalkduster")
        labelHalloRek.text = "HALO REK!"
        labelHalloRek.fontSize = 24
        labelHalloRek.position = CGPoint(x: 200, y: 400)
        labelHalloRek.color = SKColor.white
        addChild(labelHalloRek)
        
        
        LabelAjakan = SKLabelNode(fontNamed: "Chalkduster")
        LabelAjakan.text = "   Ayo Belajar Bahasa \nSuroboyo bareng si Boyo"
        LabelAjakan.fontSize = 17
        LabelAjakan.numberOfLines = 2
        LabelAjakan.lineBreakMode = NSLineBreakMode.byWordWrapping
        LabelAjakan.position = CGPoint(x: 200, y: 300)
        LabelAjakan.color = SKColor.white
        addChild(LabelAjakan)
        
    }
    
    private func bajolLogo() {
        logo = SKSpriteNode(imageNamed: "logo" )
        logo.position = CGPoint(x: 195, y: 700)
        logo.zPosition = CGFloat(1)
        logo.size = CGSize(width: 250, height: 180)
        addChild(logo)
        
        bajolAnimate()
    }
    
    private func bajolAnimate() {
        let move = SKAction.moveTo(y: 510, duration: 5)
        logo.run(move)
    }
    
    
    

}
